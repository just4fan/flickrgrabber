package com.horrrnet.flickrgrabber

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.horrrnet.flickrgrabber.databinding.FragmentImageListBinding
import com.horrrnet.flickrgrabber.databinding.ListItemImageBinding

class ImageListViewHolder(
    private val binding: ListItemImageBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(bitmap: Bitmap, onClickListener: (Bitmap) -> Unit) {
        binding.itemImage.setImageBitmap(bitmap)
        binding.itemImage.setOnClickListener { onClickListener(bitmap) }
    }
}

class ImageListAdapter(
    private val images: List<Bitmap>,
    private val onClickListener: (Bitmap) -> Unit
) : Adapter<ImageListViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemImageBinding.inflate(inflater, parent, false)
        return ImageListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageListViewHolder, position: Int) {
        holder.bind(images[position], onClickListener)
    }

    override fun getItemCount() = images.size
}

class ImageListFragment : Fragment(R.layout.fragment_image_list) {
    private var _binding: FragmentImageListBinding? = null

    private val binding: FragmentImageListBinding
        get() = checkNotNull(_binding)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentImageListBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun createCrossBitmap(): Bitmap {
        val bitmap = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_8888)
        bitmap.eraseColor(Color.WHITE)

        for(i in 0..255) {
            // Верх низ
            bitmap.setPixel(i, 0, Color.RED)
            bitmap.setPixel(i, 255, Color.RED)

            // Лево право
            bitmap.setPixel(0, i, Color.RED)
            bitmap.setPixel(255, i, Color.RED)

            // Диагонали
            bitmap.setPixel(i, i, Color.RED)
            bitmap.setPixel(i, 255 - i, Color.RED)
        }

        return bitmap
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bitmap = createCrossBitmap()

        binding.imageListView.layoutManager = GridLayoutManager(
            view.context, 2
        )

        binding.imageListView.adapter = ImageListAdapter(
            listOf(
                bitmap, bitmap, bitmap, bitmap, bitmap, bitmap, bitmap
            )
        ) {
            findNavController().navigate(ImageListFragmentDirections.showImage(it))
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}